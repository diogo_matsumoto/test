function myFunction() {
  CreateImportCardDataUI();
}

function CreateImportCardDataUI() {

    var app = UiApp.createApplication();

    // create Text Area:
    var pTextArea = CreateTextArea(app, "text", 220);
    app.add(pTextArea);

    // create a button and give it a click handler
    var pButton = CreateButton(app, "confirm", "Import");
    var pHandler = app.createServerHandler("OnClickImport").addCallbackElement(pTextArea);
    pButton.addClickHandler(pHandler);
    app.add(pButton);


    var doc = SpreadsheetApp.getActiveSpreadsheet();
    doc.show(app);
    return app;


}

function OnClickImport(eventInfo) {

  var app = UiApp.getActiveApplication();
    var pButton = app.getElementById(eventInfo.parameter.source);
  
  SpreadsheetApp.getActive().getRange("C1").setValue(JSON.stringify(eventInfo));
  
  
    
    pButton.setEnabled(false);
    pButton.setText("Processing...");

    var sJson = eventInfo.parameter.text;

  return app;
    //ImportCardData(sJson);
}


function CreateTextArea(app, sId, iHeight) {
    if (iHeight == null) {
        iHeight = 100;
    }
    var textArea = app.createTextArea().setWidth('100%').setHeight(iHeight + 'px').setId(sId).setName(sId);
    return textArea;
}

function CreateButton(app, sId, sText, sFunctionOnClick) {
    var button = app.createButton(sText);
    button.setId(sId);

    if (sFunctionOnClick != null) {
        button.addClickHandler(app.createServerHandler(sFunctionOnClick));
    }
    return button;
}


function onOpen(){
  var ui = SpreadsheetApp.getUi();
  
  ui.createMenu('Oktagon')
          .addItem('myFunction', 'myFunction')
          .addItem('TestWriteValue', 'TestWriteValue')
       .addToUi();
}
