// ************** Global vars/consts **************

var OPEN_SS_DATA;

var MSG_OBJ_CREATED_OK = "OBJ_CREATED_OK";
var MSG_TBL_CREATED_OK = "TBL_CREATED_OK";
var MSG_OBJ_DATA = "OBJ_DATA";
var MSG_TBL_DATA = "TBL_DATA";
var MSG_TBLS_DATA = "TBLS_DATA";
var MSG_OBJ_UPDT = "OBJ_UPT_OK";
var MSG_OBJ_DEL = "OBJ_DEL_OK";
var MSG_MISS_PARAM = "MISSING_PARAM";
var TYPE_END = "_ENDTYPE";
var TYPE_STRT = "TYPE_";
var MSG_MISS_SEARCH_PARAM = "SEARCH_PARAM_ERR";
var MSG_BAD_PASS = "PASS_ERROR";

var PASSWORD = "passcode"


// ************** Entry Point **************

function doPost(e)
{
  return Entry(e);
}

function doGet(e)
{
  return Entry(e);
}

// ************** Initialization functions **************

function Entry(e)
{
  // Password Check.
  if (e.parameters.pass != PASSWORD)
    return ContentService.createTextOutput(MSG_BAD_PASS);
  
   // Useful for service status quick testing.
  if (e.parameters.test != null)
    return ContentService.createTextOutput(JSON.stringify(e));
    
  // Load required stuff.
  OpenSheet(e);
  
  // Parse the request.
  var result = ParseFlow(e);
  
  // Answer the call.
  return ContentService.createTextOutput(result);
}

function OpenSheet(e)
{
  if (e.parameters.ssid == null)
    return MSG_MISS_PARAM;
  
  var SS_DATA = e.parameters.ssid.toString();
    
  // Open worksheet.  
  OPEN_SS_DATA = SpreadsheetApp.openById(SS_DATA);
}

function ParseFlow(e)
{
  var result = "";
  var action = "";
   
  if (e.parameters.action != null)
    action = e.parameters.action.toString();
  else
    return MSG_MISS_PARAM;
  
  switch (action)
  {
    case "createObject":
      result = ParseCreateObject(e);
      break;
      
    case "createTable":
      result = ParseCreateTable(e);
      break;
      
    case "getObjects":
      result = ParseGetObjects(e);
      break;
      
    case "getTable":
      result = ParseGetTable(e);
      break;
      
    case "getAllTables":
      result = ParseGetAllTables(e);
      break;
      
    case "updateObjects":
      result = ParseUpdateObjects(e);
      break;
      
    case "deleteObjects":
      result = ParseDeleteObjects(e);
      break;
  }
  
  return result;
}

function ParseCreateObject(e)
{
  if (e.parameters.type == null || e.parameters.isJson == null)
    return MSG_MISS_PARAM;
  
  if (e.parameters.isJson.toString() == "true")
    return CreateJsonObject(e);
  
  var sheet = OPEN_SS_DATA.getSheetByName(e.parameters.type.toString()); 
  var headers = sheet.getSheetValues(1, 1, 1, -1);
  var fields = [];
    
  for (var i = 0; i < headers[0].length; i++)
  {
    if (e.parameter[headers[0][i]] != null)
      fields.push(e.parameter[headers[0][i]]);
    else
      fields.push("null");
  }
  
  sheet.appendRow(fields);
  
  return MSG_OBJ_CREATED_OK;
}

function CreateJsonObject(e)
{
  if (e.parameters.type == null || e.parameters.jsonData == null)
    return MSG_MISS_PARAM;
  
  var jsonObj = JSON.parse(e.parameters.jsonData.toString());
  var sheet = OPEN_SS_DATA.getSheetByName(e.parameters.type.toString()); 
  var headers = sheet.getSheetValues(1, 1, 1, -1);
  var fields = [];
   
  for (var i = 0; i < headers[0].length; i++)
  {
    if (jsonObj.hasOwnProperty(headers[0][i]))
      fields.push(jsonObj[headers[0][i]]);
    else
      fields.push("null");
  }
  
  sheet.appendRow(fields);
  
  return MSG_OBJ_CREATED_OK;
}

function ParseCreateTable(e)
{
  if (e.parameters.type == null || e.parameters.num == null)
    return MSG_MISS_PARAM;
  
  var sheet = OPEN_SS_DATA.insertSheet(e.parameters.type.toString()); 
  var headers = [];
  var numOfFields = parseInt(e.parameters.num);
  
  for (var i = 0; i < numOfFields; i++)
  {    
    if (e.parameter["field" + i.toString()] != null)
      headers.push(e.parameter["field" + i.toString()].toString());
  }
  
  sheet.appendRow(headers);
  
  return MSG_TBL_CREATED_OK;
}

function ParseGetObjects(e)
{
  if (e.parameters.type == null || e.parameters.search == null)
    return MSG_MISS_PARAM;
  
  var field = e.parameters.search.toString();
  var value = e.parameter[field].toString();
  var sheet = OPEN_SS_DATA.getSheetByName(e.parameters.type.toString());
  var tableData = sheet.getDataRange().getValues();
  var headers = [];
  for (var h = 0; h < tableData[0].length; h++)
  {
    headers[h] = tableData[0][h];
  }
  
  // -- Determine the search field parameter.
  var fieldIndex = -1;
  for (var g = 0; g < headers.length; g++)
  {
    if (headers[g] == field)
      fieldIndex = g;
  }
  if (fieldIndex == -1)
    return MSG_MISS_SEARCH_PARAM;
  // --
    
  var objectsArray = [];
  var object = {};
  for (var i = 1; i < tableData.length; i++)
  {
    if (tableData[i][fieldIndex].toString() == value)
    {
      object = {};
      for (var j = 0; j < headers.length; j++)
      {
        object[headers[j]] = tableData[i][j];
      }
      
      objectsArray.push(object);
    }
  }
  
  var r = MSG_OBJ_DATA + "\n" + e.parameters.type.toString() + TYPE_END + "\n" + JSON.stringify(objectsArray);
  return r;
}

function ParseGetTable(e)
{
  if (e.parameters.type == null)
    return MSG_MISS_PARAM;
  
  var sheet = OPEN_SS_DATA.getSheetByName(e.parameters.type.toString());
  
  var objectsArray = TableToObjectArray(sheet);
  
  var r = MSG_TBL_DATA + "\n" + e.parameters.type.toString() + TYPE_END + "\n" + JSON.stringify(objectsArray);
  return r;
}

function ParseGetAllTables(e)
{
  var sheetsArray = OPEN_SS_DATA.getSheets();
  var result = "";
  
  for (var i = 0; i < sheetsArray.length; i++)
  {
    if (sheetsArray[i].getDataRange().isBlank())
      continue;
    
      result += "\n" + TYPE_STRT + sheetsArray[i].getName() + TYPE_END + "\n" + JSON.stringify(TableToObjectArray(sheetsArray[i]));
  }
  
  return MSG_TBLS_DATA + result;
}

function TableToObjectArray(sheet)
{ 
  if (sheet.getDataRange().isBlank())
    return "";
 
  var tableData = sheet.getDataRange().getValues();
  var headers = [];
  for (var h = 0; h < tableData[0].length; h++)
  {
    headers[h] = tableData[0][h];
  }
  var objectsArray = [];
  var object = {};
  for (var i = 1; i < tableData.length; i++)
  {
    object = {};
    for (var j = 0; j < headers.length; j++)
    {
      object[headers[j]] = tableData[i][j];
    }
    
    objectsArray.push(object);
  }
  
  return objectsArray;
}

function ParseUpdateObjects(e)
{
  if (e.parameters.type == null || 
      e.parameters.searchField == null || 
      e.parameters.searchValue == null ||
      e.parameters.updtField == null ||
      e.parameters.updtValue == null )
    return MSG_MISS_PARAM;
  
  // These two serve a search key to get to the object that needs to be updated.
  // Provide a field and value to search for.
  var searchField = e.parameters.searchField.toString(); // Field name to search for
  var searchValue = e.parameters.searchValue.toString(); // Value to search for, in the "field" described above.
  
  // These two provide the field and values to be updated.
  var updtField = e.parameters.updtField.toString();
  var updtValue = e.parameters.updtValue.toString();
  
  var sheet = OPEN_SS_DATA.getSheetByName(e.parameters.type.toString());
  
  var headers = sheet.getSheetValues(1, 1, 1, -1);
  
  // Get the index of the field to search by.
  var searchFieldIdx;
  for (var h = 0; h < headers[0].length; h++)
  {
    if (headers[0][h] == searchField)
      searchFieldIdx = h;
  }
  
  // Get the index of the field to update.
  var updtFieldIdx;
  for (var j = 0; j < headers[0].length; j++)
  {
    if (headers[0][j] == updtField)
      updtFieldIdx = j;
  }
  
  var startRow = 2;
  var rangeArray = sheet.getSheetValues(startRow, 1, sheet.getLastRow() - 1, sheet.getLastColumn());
  var length = sheet.getLastRow() - startRow;

  for (var i = 0; i <= length; i++)
  {
    if (rangeArray[i][searchFieldIdx] == searchValue)
    {
      sheet.getDataRange().getCell(i + startRow, updtFieldIdx + 1).setValue(updtValue); // "+1" is because cells are not zero based.
    }
  }
    
  return MSG_OBJ_UPDT;
}

function ParseDeleteObjects(e)
{
  if (e.parameters.type == null || e.parameters.search == null)
    return MSG_MISS_PARAM;
  
  var field = e.parameters.search.toString();
  var value = e.parameter[field].toString();
  var sheet = OPEN_SS_DATA.getSheetByName(e.parameters.type.toString());
  var fieldIndex;
  var headers = sheet.getSheetValues(1, 1, 1, -1);
    
  for (var h = 0; h < headers[0].length; h++)
  {
    if (headers[0][h] == field)
      fieldIndex = h + 1; // Cells are not zero based.
  }
    
  for (var i = 2; i <= sheet.getDataRange().getNumRows(); i++) // Start on 2, to bypass headers row.
  {
    if (sheet.getDataRange().getCell(i, fieldIndex).getValue().toString() == value)
    {
      sheet.deleteRow(i);
      i--; // getNumRows() will update the new number of rows, and the loop would accidentally ignore some rows.
    }
  }
    
  return MSG_OBJ_DEL;
}
